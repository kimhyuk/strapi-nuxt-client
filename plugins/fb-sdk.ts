import Vue from "vue";
import VueTypes from "vue-types";
import AccountKit from "./sdk.vue";

Vue.use(Vue => {
  Vue.component("facebook-account-kit", AccountKit);
});
