import Strapi from "strapi-sdk-javascript";
import Vue from "vue";

Vue.use((Vue, options) => {
  const strapi = new Strapi(process.env.apiUrl);

  Vue.prototype.$strapi = strapi;
  Vue.prototype.$apiUrl = process.env.apiUrl;
  Vue.prototype.$baseUrl = process.env.baseUrl;
});
