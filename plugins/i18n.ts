import Vue from "vue";
import VueI18n from "vue-i18n";
import { NAME } from "../store/modules/locale/constants";
import _ from "lodash";

Vue.use(VueI18n);

export default ({ app, store }) => {
	// Set i18n instance on app
	// This way we can use it in middleware and pages asyncData/fetch
	app.i18n = new VueI18n({
		locale: store.state[NAME].locale,
		fallbackLocale: "en-us",
		messages: {
			"en-us": require("~/locales/en.json"),
			"ko-kr": require("~/locales/ko.json"),
			"ja-jp": require("~/locales/jp.json"),
			"zh-cn": require("~/locales/zh.json")
		}
	});

	app.i18n.path = link => {
		if (app.i18n.locale === app.i18n.fallbackLocale) {
			return link === "/" ? link : `/${link}`;
		}

		return `/${app.i18n.locale}/${link}`;
	};

	app.i18n.cahngeLocalePath = locale => {
		if (locale === app.i18n.locale) return window.location.pathname;
		const splits = window.location.pathname.split("/");

		let next = "";
		let start = splits.length > 1 ? 2 : splits.length;

		if (app.i18n.locale === app.i18n.fallbackLocale) {
			start = 1;
		}

		if (locale !== app.i18n.fallbackLocale) {
			next = "/" + locale;
		}

		return next + "/" + _.slice(splits, start).join("/");
	};
};
