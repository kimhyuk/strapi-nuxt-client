module.exports = {
  baseUrl: `${process.env.BASE_URL || "http://cryptamble.net"}`,
  apiUrl: `${process.env.API_URL || "http://api.cryptamble.net"}`
};
