import NuxtConfiguration from "@nuxt/config";
import webpack from "webpack";

let env = {};

if (process.env.NODE_ENV === "production") {
  env = require("./env/production");
} else {
  env = require("./env/develoment");
}

console.log(env);
const config: NuxtConfiguration = {
  mode: "spa",
  head: {
    title: "Cryptamble",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Cryptamble project" }
    ],
    link: [{ rel: "icon", type: "image/jpeg", href: "/logo.jpg" }]
  },
  env: env,
  css: [
    "@mdi/font/css/materialdesignicons.css",
    "@fortawesome/fontawesome-free/css/all.css",
    "~/assets/scss/main.scss"
  ],
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  router: {
    middleware: "i18n"
  },
  plugins: [
    "~/plugins/i18n.ts",
    "~/plugins/strapi.ts",
    "~/plugins/fb-sdk.ts",
    "~plugins/flagIcon.ts",
    "~plugins/clipboard.ts"
  ],
  modules: [
    "@nuxtjs/vuetify",
    "@nuxtjs/moment",
    "@nuxtjs/style-resources",
    [
      "nuxt-validate",
      {
        lang: "en",
        nuxti18n: {
          locale: {
            "en-us": "en",
            "zh-cn": "zh_CN",
            "ja-jp": "ja",
            "ko-kr": "ko_KR"
          }
        }
        // regular vee-validate options
      }
    ]
  ],
  styleResources: {
    scss: ["~/assets/scss/main.scss"]
  },

  serverMiddleware: ["ts-node"],

  vuetify: {
    /// Vuetify options
    //  theme: { }
    theme: {
      primary: "#122940",
      secondary: "#07374f"
    },
    icons: {
      iconfont: "fa" // default - only for display purposes
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config: webpack.Configuration, { isDev, isClient }) {
      if (isDev && isClient && config.module) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(ts|vue)$/,
          loader: "tslint-loader",
          exclude: /(node_modules)/,
          options: {
            appendTsSuffixTo: [/\.vue$/]
          }
        });
      }
    }
  }
};

export default config;
