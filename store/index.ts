import Vuex, { StoreOptions } from "vuex";
import modules from "./modules";
import { IRootState } from "./types";

const store: StoreOptions<IRootState> = {
  state: {
    version: "1.0.0"
  },
  modules
};

const instance = new Vuex.Store<IRootState>(store);

export default () => {
  return instance;
};
