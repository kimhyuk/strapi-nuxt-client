import { ActionTree } from "vuex";
import { ACTIONS, MUTATIONS } from "./constants";
import { ILocaleState } from "./types";
import { IRootState } from "@/store/types";

const actions: ActionTree<ILocaleState, IRootState> = {
  [ACTIONS.SET_LANG]({ commit }, locale: string) {
    commit(MUTATIONS.SET_LANG, locale);
  }
};

export default actions;
