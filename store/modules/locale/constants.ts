const COMMON = {
  SET_LANG: "SET_LANG"
};

export const ACTIONS = {
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {};

export const NAME = "locale";

export default {
  ACTIONS,
  MUTATIONS,
  NAME
};
