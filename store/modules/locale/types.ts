export interface ILocaleState {
  locales: Array<string>;
  locale: string;
}
