import { MutationTree } from "vuex";
import { ILocaleState } from "./types";
import { ACTIONS, MUTATIONS } from "./constants";

// mutations
const mutations: MutationTree<ILocaleState> = {
  [ACTIONS.SET_LANG](state, locale: string) {
    if (state.locales.includes(locale)) {
      state.locale = locale;
    }
  }
};

export default mutations;
