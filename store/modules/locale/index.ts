import { IRootState } from "@/store/types";
import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import { ILocaleState } from "./types";
import constants from "./constants";

const locale: Module<ILocaleState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations
};

export { ILocaleState, constants };

export default locale;
