import { ILocaleState } from "./types";

const state: ILocaleState = {
  locale: "en-us",
  locales: ["en-us", "ko-kr", "ja-jp", "zh-cn"]
};

export default () => state;
