import user from "./user";
import locale from "./locale";
import dashboard from "./dashboard";

export default {
  user,
  locale,
  dashboard
};
