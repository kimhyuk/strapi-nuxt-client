import { MutationTree } from "vuex";
import { IUserState, IUser, User } from "./types";
import { ACTIONS, MUTATIONS } from "./constants";

// mutations
const mutations: MutationTree<IUserState> = {
  [MUTATIONS.SET_USER](state, user: IUser) {
    state.user = user;
  },
  [MUTATIONS.SET_JWT](state, jwt: string) {
    state.jwt = jwt;
  },
  [MUTATIONS.DELETE_USER](state) {
    state.user = undefined;
  },
  [MUTATIONS.DELETE_JWT](state) {
    state.jwt = "";
  },
  [MUTATIONS.INIT](state, { user, jwt }) {
    if (jwt) {
      state.jwt = jwt;
    }
    if (user) {
      state.user = user;
    }
  }
};

export default mutations;
