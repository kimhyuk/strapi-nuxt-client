import { IUserState } from "./types";

const state: IUserState = {
  jwt: "",
  user: undefined
};

export default () => state;
