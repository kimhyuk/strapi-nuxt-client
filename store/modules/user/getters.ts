import { GetterTree } from "vuex";
import { IUserState, IUser, User } from "./types";
import { ACTIONS, MUTATIONS, GETTERS } from "./constants";
import { IRootState } from "~/store/types";
// getters
const getters: GetterTree<IUserState, IRootState> = {
  [GETTERS.GET_TOKEN]({ jwt }) {
    return jwt ? `Bearer ${jwt}` : undefined;
  }
};
export default getters;
