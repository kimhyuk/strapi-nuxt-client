import { IRootState } from "@/store/types";
import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import { IUserState } from "./types";
import constants from "./constants";

const user: Module<IUserState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};

export { IUserState, constants };

export default user;
