import { ActionTree } from "vuex";
import { ACTIONS, MUTATIONS, NAME } from "./constants";
import { IUserState, IUser } from "./types";
import { IRootState } from "@/store/types";
import Cookies from "js-cookie";

const actions: ActionTree<IUserState, IRootState> = {
  [ACTIONS.SET_USER]({ commit }, user: IUser) {
    Cookies.set("user", user);
    commit(MUTATIONS.SET_USER, user);
  },
  [ACTIONS.SET_JWT]({ commit }, jwt: string) {
    Cookies.set("jwt", jwt);
    commit(MUTATIONS.SET_JWT, jwt);
  },
  [ACTIONS.DELETE_USER]({ commit }) {
    Cookies.remove("user");
    commit(MUTATIONS.DELETE_USER);
  },
  [ACTIONS.DELETE_JWT]({ commit }) {
    Cookies.remove("jwt");
    commit(MUTATIONS.DELETE_JWT);
  },
  [ACTIONS.INIT]({ commit }) {
    const jwt = Cookies.get("jwt");
    const user = Cookies.getJSON("user");

    commit(MUTATIONS.INIT, { jwt, user });
  },
  [ACTIONS.LOGOUT]({ commit, dispatch }) {
    dispatch(ACTIONS.DELETE_USER);
    dispatch(ACTIONS.DELETE_JWT);
  }
};

export default actions;
