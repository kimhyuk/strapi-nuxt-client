export interface IUserState {
  user?: IUser;
  jwt: string;
}

export interface IUser {
  email: string;
  username: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  address: string;
  receiveAddress: string;
  phone: string;
  referralCode: string;
  referralCount: number;
  referral: string;
  estimatedAmountOfInvestment?: Number | null;
}

export class User implements IUser {
  email = "";
  username = "";
  provider = "";
  confirmed = true;
  blocked = false;
  address = "";
  receiveAddress = "";
  phone = "";
  referral = "";
  referralCode = "";
  referralCount = 0;
  estimatedAmountOfInvestment = 0;

  constructor(user?: IUser) {
    if (user) {
      this.email = user.email;
      this.provider = user.provider;
      this.username = user.username;
      this.confirmed = user.confirmed;
      this.blocked = user.blocked;
      this.address = user.address;
      this.referral = user.referral;
      this.referralCode = user.referralCode;
      this.receiveAddress = user.receiveAddress;
      this.referralCount = user.referralCount;
      if (typeof user.estimatedAmountOfInvestment === "number") {
        this.estimatedAmountOfInvestment = user.estimatedAmountOfInvestment;
      }
    }
  }

  isLogin(): boolean {
    return this.email ? true : false;
  }
}

export class SignupUser extends User {
  password = "";
  passwordConfirm = "";

  constructor(user?: SignupUser | User) {
    super(user);
    if (user instanceof SignupUser) {
      this.password = user.password;
      this.passwordConfirm = user.passwordConfirm;
    }
  }
}
