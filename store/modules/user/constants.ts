const COMMON = {
  SET_USER: "SET_USER",
  SET_JWT: "SET_JWT",
  DELETE_USER: "DELETE_USER",
  DELETE_JWT: "DELETE_JWT",
  LOGOUT: "LOGOUT",
  INIT: "INIT"
};

export const ACTIONS = {
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {
  GET_TOKEN: "GET_TOKEN",
  GET_USER: "GET_USER"
};

export const NAME = "user";

export default {
  ACTIONS,
  MUTATIONS,
  NAME
};
