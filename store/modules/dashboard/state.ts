import { IDashboardState } from "./types";

const state: IDashboardState = {
  snackbar: {
    show: false,
    text: ""
  }
};

export default () => state;
