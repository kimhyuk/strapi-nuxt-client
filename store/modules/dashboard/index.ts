import { IRootState } from "@/store/types";
import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import { IDashboardState } from "./types";
import constants from "./constants";

const deshboard: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations
};

export { IDashboardState, constants };

export default deshboard;
