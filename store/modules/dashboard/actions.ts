import { ActionTree } from "vuex";
import { ACTIONS, MUTATIONS } from "./constants";
import { IDashboardState } from "./types";
import { IRootState } from "@/store/types";

const actions: ActionTree<IDashboardState, IRootState> = {
  [ACTIONS.SET_SNACBAR]({ commit }, snackbar: object) {
    commit(MUTATIONS.SET_SNACBAR, snackbar);
  }
};

export default actions;
