export interface IDashboardState {
  snackbar: {
    show: boolean;
    text: string;
  };
}
