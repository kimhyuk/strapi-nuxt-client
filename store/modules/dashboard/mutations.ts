import { MutationTree } from "vuex";
import { IDashboardState } from "./types";
import { ACTIONS, MUTATIONS } from "./constants";

// mutations
const mutations: MutationTree<IDashboardState> = {
  [ACTIONS.SET_SNACBAR](state, snackbar) {
    state.snackbar = snackbar;
  }
};

export default mutations;
