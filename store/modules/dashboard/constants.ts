const COMMON = {
  SET_SNACBAR: "SET_SNACBAR"
};

export const ACTIONS = {
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {};

export const NAME = "dashboard";

export default {
  ACTIONS,
  MUTATIONS,
  NAME
};
