import Vue from "vue";

declare module "@nuxt/vue-app" {
  interface Context {
    $vutify: any;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $vutify: any;
  }
}
